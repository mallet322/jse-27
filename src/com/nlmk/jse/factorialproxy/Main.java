package com.nlmk.jse.factorialproxy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    private static final Logger logger = Logger.getLogger(Main.class.getName());

    private static final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) {

        Factorial factorial = new Factorial();

        InvocationHandler handler = new FactorialHandler(factorial);

        IFactorial proxy = (IFactorial) Proxy.newProxyInstance(
                factorial.getClass().getClassLoader(), factorial.getClass().getInterfaces(), handler);

        while (true) {
            try {
                String value = reader.readLine();
                if ("end".equals(value)) {
                    logger.log(Level.INFO, "Ending");
                    break;
                } else {
                    final int number = Integer.parseInt(value);
                    System.out.println(proxy.getFactorial(number));
                }
            } catch (IOException e) {
                logger.log(Level.WARNING, "Error: ".concat(e.toString()));
            }
        }
    }

}