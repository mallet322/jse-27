package com.nlmk.jse.factorialproxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Map;

public class FactorialHandler implements InvocationHandler {

    private static final int MAX_CAPACITY = 10;

    private final Factorial factorial;

    private final Map<Integer, Integer> factorialMap;

    public FactorialHandler(Factorial factorial) {
        this.factorial = factorial;
        this.factorialMap = new LinkedHashMap<>() {
            @Override
            protected boolean removeEldestEntry(Map.Entry<Integer, Integer> eldest) {
                return size() > MAX_CAPACITY;
            }
        };
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if ("getFactorial".equals(method.getName())) {
            Integer argument = (Integer) args[0];
            if (factorialMap.containsKey(argument)) {
                return factorialMap.get(argument);
            }
            Integer result = (Integer) method.invoke(factorial, args);
            factorialMap.put(argument, result);
            return result;
        }
        return method.invoke(factorial, args);
    }

}