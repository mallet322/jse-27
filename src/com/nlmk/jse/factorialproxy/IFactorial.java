package com.nlmk.jse.factorialproxy;

public interface IFactorial {

    int getFactorial(int num);

}