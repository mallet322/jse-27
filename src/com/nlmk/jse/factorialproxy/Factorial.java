package com.nlmk.jse.factorialproxy;

public class Factorial implements IFactorial {

    @Override
    public int getFactorial(int num) {
        int result = 1;
        for (int i = 1; i <= num; i++) {
            result = result * i;
        }
        return result;
    }

}